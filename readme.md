<p align="center"><img src="https://chessageme.com/images/chessageme_logo.png"></p>

## About Chessageme

Chessageme is a basic web application that makes use of the <a href="https://lichess.org/api">lichess API</a> to email users when a lichess player of their choice starts playing on <a href="https://lichess.org/">lichess.org</a>. 
